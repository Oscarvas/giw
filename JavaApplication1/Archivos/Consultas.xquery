xquery version "3.0";


let $consultas:=
<consultas>
   <consulta id="1"> <Direcciones>{
      
    for $b in /GeocodeResponse/result
    return 
          <Direccion>
            {
                if ($b/type = "university") then "&#10; Universidad Complutense de Madrid"
                else "&#10; Universidad de Zaragoza"
            }
            <Pais>{data($b/address_component[type = "country"]/long_name)}</Pais>
            <Ciudad>{data($b/address_component[type = "locality"]/long_name)}</Ciudad>
            <Codigo_Postal>{data($b/address_component[type = "postal_code"]/long_name)}</Codigo_Postal>
            
          </Direccion>
    }
    </Direcciones>
        
    </consulta>
    
    <consulta id="2">
    <Coordenadas>{
    for $b in /GeocodeResponse/result
    return
          <Universidad>
             {
                if ($b/type = "university") then <Nombre>Universidad Complutense de Madrid</Nombre>
                else <Nombre>Universidad de Zaragoza</Nombre>
            } 
             <Latitud>{data($b/geometry/location/lat)} </Latitud>
             <Longitud>{data($b/geometry/location/lng)}</Longitud>
          </Universidad>
    }
    </Coordenadas></consulta>
    
    
    
    
    <consulta id="3">
    <infor_admin>{
      
    for $b in /GeocodeResponse/result order by $b/type[1] descending
    return
          <Universidad>
             {
                if ($b/type = "university") then <Nombre>Universidad Complutense de Madrid</Nombre>
                else <Nombre>Universidad de Zaragoza</Nombre>
            }
            <Nivel_1>{
                if ($b/address_component[type = "administrative_area_level_1"]) then data($b/address_component[type = "administrative_area_level_1"]/long_name)
                else "No consta"
            }
            </Nivel_1>
            <Nivel_2>{
                if ($b/address_component[type = "administrative_area_level_2"]) then data($b/address_component[type = "administrative_area_level_2"]/long_name)
                else "No consta"
            }</Nivel_2>
            <Nivel_3>{
                if ($b/address_component[type = "administrative_area_level_3"]) then data($b/address_component[type = "administrative_area_level_3"]/long_name)
                else "No consta"
            }</Nivel_3>
            <Nivel_4>{
                if ($b/address_component[type = "administrative_area_level_4"]) then data($b/address_component[type = "administrative_area_level_4"]/long_name)
                else "No consta"
            }</Nivel_4>
            
          </Universidad>
    }
    </infor_admin></consulta>
    
    <consulta id="4">
    <agrega>{
    for $b in /GeocodeResponse/result
        let $count := $b/address_component
            return 
              <Universidad>
                {
                    if ($b/type = "university") then <Nombre>Universidad Complutense de Madrid</Nombre>
                    else <Nombre>Universidad de Zaragoza</Nombre>
                }
                 <Tipo>{data($b/type[1])} </Tipo>
                 <Tipo>{data($b/type[2])}</Tipo>
                 <num_nodos_address> {count($count)}</num_nodos_address>
                 
             </Universidad>
                
    }
    </agrega></consulta>
    
    <consulta id="5">
    <distancia>{
    let $latmadrid:=/GeocodeResponse/result[address_component/long_name="Madrid"]/geometry/location/lat
    let $latzaragoza:=/GeocodeResponse/result[address_component/long_name="Zaragoza"]/geometry/location/lat
    let $longmadrid:=/GeocodeResponse/result[address_component/long_name="Madrid"]/geometry/location/lng
    let $longzaragoza:=/GeocodeResponse/result[address_component/long_name="Zaragoza"]/geometry/location/lng
    
    let $kmlat:=10000
    let $kmlong:=111.1111
    
    let $puntoA:= (($latmadrid * $kmlat) div 90) - (($latzaragoza * $kmlat) div 90)
    let $puntoB:= ($longmadrid * $kmlong) - ($longzaragoza * $kmlong)
    
    let $distancia:= math:sqrt( math:pow($puntoA,2) + math:pow($puntoB,2))
    
    
    return $distancia
        }
    </distancia></consulta>
</consultas>

for $x in $consultas//consulta order by $x/@id ascending
    return $x/*


    
    
