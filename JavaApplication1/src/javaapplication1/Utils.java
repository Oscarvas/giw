/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Database;
import org.xmldb.api.base.XMLDBException;

/**
 *
 * @author rpax
 */
public class Utils {
    
    public static final String DB_CONN_STRING="xmldb:exist://localhost:8080/exist/xmlrpc/db";
    public static final String DB_USERNAME = "admin";
    public static final String DB_PASS = "eXist";
    private static boolean DRIVER_REGISTERED=false;
    public static void registerDriver() throws ClassNotFoundException, InstantiationException, XMLDBException, IllegalAccessException {
        if (DRIVER_REGISTERED) return;
        String driver = "org.exist.xmldb.DatabaseImpl"; 
        //Cargamos el driver.
        Class cl = Class.forName(driver);
        //Creamos una nueva instancia de la base de datos.
        Database database = (Database) cl.newInstance(); 
        database.setProperty("create-database", "true");
        //Registramos la base de datos.
        DatabaseManager.registerDatabase(database); 
        DRIVER_REGISTERED=true;
    }
}
