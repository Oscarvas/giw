package javaapplication1;

import java.io.*;
import java.util.Scanner;

import org.xmldb.api.*;
import org.xmldb.api.base.*;
import org.xmldb.api.modules.*;
import static javaapplication1.Utils.DB_CONN_STRING;
import static javaapplication1.Utils.DB_USERNAME;
import static javaapplication1.Utils.DB_PASS;

public class JavaApplication1 {

    public static void main(String args[]) throws Exception {

        //Buscamos guardar los argumentos de entrada en en array para tratarlos luego
        String[] cadenaComoArray = new Scanner(System.in).nextLine().split(" ");
        if (cadenaComoArray.length != 4) {
            throw new RuntimeException("Sólo 4 argumentos");
        }

        String collection = cadenaComoArray[0];
        String fileMadrid = cadenaComoArray[1];
        String fileZaragoza = cadenaComoArray[2];
        String fileXQuery = cadenaComoArray[3];
//Eliminar “/db” si es especificado en la entrada
        if (collection.startsWith("/db")) {
            collection = collection.substring(3);
        }
// Se inicializa el driver
        Utils.registerDriver();
// Se intenta acceder a la colección específicada como primer argumento
        Collection col = DatabaseManager.getCollection(DB_CONN_STRING + collection, DB_USERNAME, DB_PASS);
        if (col == null) {
// Si la colección especificada como primer argumento no existe, entonces se crea
            Collection root = DatabaseManager.getCollection(DB_CONN_STRING, DB_USERNAME, DB_PASS);
            CollectionManagementService mgtService = (CollectionManagementService) root.getService("CollectionManagementService", "1.0");
            mgtService.setProperty("encoding", "ISO-8859-1");
            col = mgtService.createCollection(collection);
        }
// Se crea un recurso XML para almacenar el documento
        File f_madrid = new File(fileMadrid);
        XMLResource documentMadrid = (XMLResource) col.createResource(f_madrid.getName() , "XMLResource");

        
        if (!f_madrid.canRead()) {
            System.err.println("No es posible leer el archivo " + fileMadrid);
        }

        documentMadrid.setContent(f_madrid);
        System.out.println("Almacenando el archivo " + documentMadrid.getId() + "...");
        col.storeResource(documentMadrid);
        System.out.println("Almacenado correctamente");

        File f_zaragoza = new File(fileZaragoza);
        XMLResource documentZaragoza = (XMLResource) col.createResource(f_zaragoza.getName(), "XMLResource");
        
        if (!f_zaragoza.canRead()) {
            System.err.println("No es posible leer el archivo " + fileZaragoza);
        }

        documentZaragoza.setContent(f_zaragoza);
        System.out.println("Almacenando el archivo " + documentZaragoza.getId() + "...");
        col.storeResource(documentZaragoza);
        System.out.println("Almacenado correctamente");
        System.out.println("Ejecutando consultas");

        String resultado = XQueryExecuter.executeFromFile(fileXQuery);
        // guardamos en fichero
        try (PrintStream out = new PrintStream(new FileOutputStream("Resultados.txt"))) {
            out.print(resultado);
        }

    }
}
