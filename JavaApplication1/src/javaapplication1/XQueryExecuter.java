/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.xmldb.api.DatabaseManager;
import org.xmldb.api.base.Collection;
import org.xmldb.api.base.ResourceIterator;
import org.xmldb.api.base.ResourceSet;
import org.xmldb.api.base.XMLDBException;
import org.xmldb.api.modules.XPathQueryService;

/**
 *
 * @author rpax
 */
public class XQueryExecuter {

    private static String readFile(String fileName) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String line;
            StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line).append('\n');
            }
            return sb.toString();
        }
    }

    public static String  executeFromFile(String fileName) throws ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException, IOException {
        return execute(readFile(fileName));
    }

    public static String execute(String query) throws ClassNotFoundException, InstantiationException, IllegalAccessException, XMLDBException {
        Utils.registerDriver();
        //Accedemos a la colección
        Collection col = DatabaseManager.getCollection("xmldb:exist://localhost:8080/exist/xmlrpc/db/", "admin", "eXist");
        XPathQueryService service = (XPathQueryService) col.getService("XPathQueryService", "1.0");
        service.setProperty("pretty", "true");
        service.setProperty("encoding", "ISO-8859-1");
       
        //Se lanza la consulta
        ResourceSet result = service.query(query);
        
      
      System.out.println(result.getSize());
        ResourceIterator it = result.getIterator();
        //Se procesa el resultado.
        int i=0;
        StringBuilder sb = new StringBuilder();
        sb.append("RESULTADOS CONSULTAS XQUERY\n");
        sb.append("===========================\n");
        while (it.hasMoreResources()) {
          sb.append("Consulta ").append(++i).append(": \n");
          sb.append(it.nextResource().getContent()).append(" \n");
        }
        return sb.toString();
    }

}
